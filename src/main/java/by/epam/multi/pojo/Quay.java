package by.epam.multi.pojo;


import org.apache.log4j.Logger;


/**
 * This class encapsulates an instance of the quay.
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public class Quay {
    private int quayId;

    static Logger logger = Logger.getLogger(Quay.class);

    public Quay(int quayId) {
	this.quayId = quayId;
    }

    public int getQuayId() {
	return quayId;
    }

    public void setQuayId(int quayId) {
	this.quayId = quayId;
    }
}
