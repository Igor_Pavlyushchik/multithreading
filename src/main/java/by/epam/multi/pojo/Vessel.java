package by.epam.multi.pojo;

import by.epam.multi.exception.TechnicalException;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * This class encapsulates an instance of the vessel, which is implemented as separate thread of the application.
 * Each vessel asks the port for the quay and on receiving one invokes loading and unloading operations on the
 * instance of the {@code PortService}, finally the quay is returned to the port pool of quays.
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public class Vessel extends Thread {
    private static final int SS_CAPACITY = 10;
    private static final int NUMBER_OF_OPERATIONS = 3;
    private String vesselName;
    private int numberOfContainers;
    private int capacity;
    private Port port;
    private Operation operation;

    static Logger logger = Logger.getLogger(Vessel.class);

    public Vessel(String vesselName, Port port) {
	Random random = new Random();
	this.vesselName = vesselName;
	capacity = SS_CAPACITY;
	this.port = port;
	operation = Operation.values()[random.nextInt(NUMBER_OF_OPERATIONS)];
	switch (operation) {
	    case UNLOADING:
	    case UNLOADING_AND_LOADING:
		numberOfContainers = random.nextInt(SS_CAPACITY) + 1;
		break;
	    case LOADING:
		numberOfContainers = 0;
	}
    }

    public Operation getOperation() {
	return operation;
    }

    public void setOperation(Operation operation) {
	this.operation = operation;
    }

    public String getVesselName() {
	return vesselName;
    }

    public void setVesselName(String vesselName) {
	this.vesselName = vesselName;
    }

    public int getNumberOfContainers() {
	return numberOfContainers;
    }

    public void setNumberOfContainers(int numberOfContainers) {
	this.numberOfContainers = numberOfContainers;
    }

    public int getCapacity() {
	return capacity;
    }

    public void setCapacity(int capacity) {
	this.capacity = capacity;
    }

    public Port getPort() {
	return port;
    }

    public void setPort(Port port) {
	this.port = port;
    }

    public void run() {
	 Quay quay = null;
	try {
	    quay = port.getQuay();
	    logger.info(getVesselName() + " is now at " + quay.getQuayId() + " quay.");
	    port.getPortService().loadingUnloading(numberOfContainers, port, operation, SS_CAPACITY);
	} catch (TechnicalException e) {
	    logger.error("Problem trying to acquire access to the quay. " + e);
	} finally {
	    if(quay != null) {
		logger.info(getVesselName() + " has finished unloading at " + quay.getQuayId() + " quay.");
		port.returnQuay(quay);
		logger.info("Quay " + quay.getQuayId() +" is returned to pool.");
	    }
	}
    }
}
