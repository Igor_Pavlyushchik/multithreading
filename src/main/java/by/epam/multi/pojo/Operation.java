package by.epam.multi.pojo;

/**
 * This enum holds types of operations which are performed in the {@code Port}.
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public enum Operation {
    UNLOADING, LOADING, UNLOADING_AND_LOADING;
}
