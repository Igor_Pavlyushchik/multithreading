package by.epam.multi.pojo;

import by.epam.multi.exception.TechnicalException;
import by.epam.multi.service.PortService;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class encapsulates the port singleton instance which has the number of the containers in the port and
 * provides functionality of assigning quays to the arrived threads - vessels.
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public class Port {
    private final static int INIT_CONTAINERS = 100;           // initial number of the containers in the Port
    private final Queue<Quay> quays = new LinkedList<>();
    private PortService portService = new PortService();
    private AtomicInteger numberContainersPort = new AtomicInteger(INIT_CONTAINERS);
    private static Port port = null;
    private static volatile boolean portCreated = false;
    private static ReentrantLock lock = new ReentrantLock();
    private final Condition quaysFree = lock.newCondition();
    static Logger logger = Logger.getLogger(Port.class);

    private Port(Queue<Quay> source) {
	quays.addAll(source);
    }

    public static Port getPort(Queue<Quay> source) {
	if(!portCreated) {
	    lock.lock();
	    try {
		if(!portCreated) {
		    port = new Port(source);
		    portCreated = true;
		}
	    }  finally {
		lock.unlock();
	    }
	}
	return port;
    }

    public PortService getPortService() {
	return portService;
    }

    public AtomicInteger getNumberContainersPort() {
	return numberContainersPort;
    }

    public void setNumberContainersPort(AtomicInteger numberContainersPort) {
	this.numberContainersPort = numberContainersPort;
    }

    /**
     * This method assigns an available quay to the vessel. If there are no vacant quays the thread {@code Vessel}
     * is put on hold, until one be released.
     * @return  an instance of {@code Quay}
     * @throws TechnicalException when there is a problem with the {@code await} method
     */
    public Quay getQuay() throws TechnicalException {
	lock.lock();
	try {
	    Quay quay = quays.poll();
	    while(quay == null) {
		logger.info(((Vessel)Thread.currentThread()).getVesselName() + " is waiting");
		try {
		    quaysFree.await();
		} catch (InterruptedException e) {
		    throw new TechnicalException("The thread is interrupted while calling await(). " + e);
		}
		quay = quays.poll();
	    }
	    return quay;
	} finally {
	    lock.unlock();
	}
    }

    /**
     * This method returns a quay, released by the vessel, to the pool of available quays.
     * @param quay  Given instance of {@code Quay} is returned to the pool of available quays, when the vessel
     *              finished its' operations (loading - unloading) in the port.
     */
    public void returnQuay(Quay quay) {
	lock.lock();
	try {
	    quays.add(quay);
	} finally {
	    quaysFree.signal() ;
	    lock.unlock();
	}
    }
}
