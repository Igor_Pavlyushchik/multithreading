package by.epam.multi.main;

import by.epam.multi.creator.Creator;
import by.epam.multi.pojo.Port;
import by.epam.multi.pojo.Quay;
import by.epam.multi.pojo.Vessel;
import by.epam.multi.reporter.Reporter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;

/**
 * <p>This is the main class of the multithreaded console application presenting simple model of the sea port
 * functioning. The cargo vessels, represented by the individual threads arrive to the port with certain number of
 * the containers and have a purpose of visit: unloading, loading or both. The Port has several quays. Each of them
 * can accept only one ship at a time. Also the port has a certain stock of the containers, which is changed
 * by each vessel.</p>
 * <p>The console report on the number of container in the port is created after in the beginning and at the end
 * of the application.</p>
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public class PortRunner {
    private static final int NUMBER_OF_VESSELS = 10;
    private final static int NUMBER_OF_QUAYS = 3;
    private final static String CONTAINERS_BEGINNING = "Number of the containers in the port on opening: ";
    private final static String CONTAINERS_END = "Number of the containers in the port before closing: ";
    public static final String LOG4J = "/logger/log4j.properties";

    static {
	Properties logProperties = new Properties();
	try {
	    logProperties.load(PortRunner.class.getResourceAsStream(LOG4J));
	    PropertyConfigurator.configure(logProperties);
	} catch (IOException e) {
	    throw new ExceptionInInitializerError("Error trying to initialize logger." + e);
	}
    }
    static Logger logger = Logger.getLogger(PortRunner.class);

    public static void main(String[] args) {

	logger.info("Port started.");
	Creator creator = new Creator();
	Reporter reporter = new Reporter();
	// Create instances of the Port quays.
	LinkedList<Quay> quays = creator.createQuays(NUMBER_OF_QUAYS);

	// Create the port.
	Port port = Port.getPort(quays);
	logger.info("Number of the containers in the port on opening: " + port.getNumberContainersPort());
	reporter.printContainersNumberInPort(port, CONTAINERS_BEGINNING);

	// Create an array of vessels which will arrive to the port during it functioning.
	Vessel [] vessels = new Vessel[NUMBER_OF_VESSELS];
	// Let's generate some vessels with the help of Creator class.
	creator.generateVessels(vessels, port);

	// Make main thread waiting for all Vessel threads terminate.
	creator.joinVessels(vessels);

	logger.info("Number of the containers in the port before closing: " + port.getNumberContainersPort());
	reporter.printContainersNumberInPort(port, CONTAINERS_END);
	logger.info("Port closed.");
    }
}
