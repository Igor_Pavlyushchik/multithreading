package by.epam.multi.reporter;

import by.epam.multi.pojo.Port;

/**
 * This class is created to report the results of the multithreaded task Port.
 * @author Igor Pavlyushchik
 *         Created on March 17, 2015.
 */
public class Reporter {
    /**
     * This method outputs to the console number of the containers in the port.
     * @param port  Singleton instance of the {@code Port}.
     * @param info  message to be displayed with the number of containers.
     */
    public void printContainersNumberInPort(Port port, String info) {
	System.out.println(info + port.getNumberContainersPort());
    }
}
