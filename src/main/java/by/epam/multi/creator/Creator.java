package by.epam.multi.creator;

import by.epam.multi.pojo.Port;
import by.epam.multi.pojo.Quay;
import by.epam.multi.pojo.Vessel;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Random;

/**
 * This class creates perform some initialization functions for the application.
 * @author Igor Pavlyushchik
 *         Created on March 15, 2015.
 */
public class Creator {
    private final static int INTERVAL_BETWEEN_VESSELS = 200;
    static Logger logger = Logger.getLogger(Creator.class);

    /**
     * This method creates and initializes the queue of quays for the application.
     * @param numberOfQuays  Number of quays in the port.
     * @return  The {@code LinkedList} of quays.
     */
    public LinkedList<Quay> createQuays(int numberOfQuays) {
	LinkedList<Quay> quays = new LinkedList<>();
	for (int i = 0; i < numberOfQuays; i++) {
	     quays.add(new Quay(i));
	}
	return quays;
    }

    /**
     * This method creates, initializes and starts threads - vessels.
     * @param vessels an empty array of type {@code Vessel}
     * @param port The port of destination.
     */
    public void generateVessels(Vessel[] vessels, Port port) {
	for (int i = 0; i < vessels.length; i++) {
	    vessels[i] = new Vessel("SS # " + i, port);
	    vessels[i].setName(String.valueOf(i));
	    vessels[i].start();
	    logger.info(vessels[i].getVesselName() + " has arrived with " + vessels[i].getNumberOfContainers()
		+ " containers, with the purpose of "
		+ vessels[i].getOperation().toString().toLowerCase().replace("_", " ") + ".");
	    try {
		Thread.sleep((new Random()).nextInt(INTERVAL_BETWEEN_VESSELS));
	    } catch (InterruptedException e) {
		logger.error("Main thread is interrupted.");
	    }
	}
    }

    /**
     * This method insures that the main thread will end after all child-threads, to display final result.
     * @param vessels An array with all the threads - vessels of the application.
     */
    public void joinVessels(Vessel[] vessels) {
	for (int i = 0; i < vessels.length; i++) {
	    try {
		vessels[i].join();
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}
    }
}
