package by.epam.multi.service;

import by.epam.multi.pojo.Operation;
import by.epam.multi.pojo.Port;
import org.apache.log4j.Logger;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class is responsible for calculating operations made with the containers in the port
 * {@link by.epam.multi.pojo.Port#numberContainersPort} and arriving vessels.
 * @author Igor Pavlyushchik
 *         Created on March 17, 2015.
 */
public class PortService {
    static Logger logger = Logger.getLogger(PortService.class);
    private static final int OPERATION_TIME = 500;         // mock the port operation length
    private static ReentrantLock lock = new ReentrantLock();

    /**
     * <p> This method is responsible for unloading and loading operations in the port for the current {@code Vessel}
     * @param containersOnVessel  Number of the containers on the vessel.
     * @param port   {@code Port} to which the vessel arrived.
     * @param operation  enum of one of the possible {@link by.epam.multi.pojo.Operation} types <br>
     *                  ({@code UNLOADING} {@code LOADING} {@code UNLOADING_AND_LOADING}.
     * @param capacity  vessel capacity in the number of containers.
     */
    public void loadingUnloading(int containersOnVessel, Port port, Operation operation, int capacity) {
	lock.lock();
	try {
	    logger.debug("before operation " + port.getNumberContainersPort() + " " + operation.toString());
	    switch (operation) {
		case UNLOADING:
		    unload(containersOnVessel, port);
		    break;
		case LOADING:
		    load(port, capacity);
		    break;
		case UNLOADING_AND_LOADING:
		    unload(containersOnVessel, port);
		    load(port, capacity);
	    }
	    logger.debug("after  operation " + port.getNumberContainersPort());
	try {
	    Thread.sleep(OPERATION_TIME);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	}  finally {
	    lock.unlock();
	}
    }

    /**
     * This method calculates unloading of the of vessel.
     * @param containersOnVessel  Number of the containers on the vessel
     * @param port  {@code Port} to which the vessel arrived.
     */
    private void unload(int containersOnVessel, Port port) {
	port.getNumberContainersPort().set(port.getNumberContainersPort().get() + containersOnVessel);
    }

    /**
     * This method calculates loading of the vessel, generating the amount of the containers to be loaded
     * within the limits of the vessel capacity.
     * @param port   {@code Port} to which the vessel arrived.
     * @param capacity  vessel capacity in the number of containers
     */
    private void load(Port port, int capacity) {
	int containersForLoading = (new Random()).nextInt(capacity) + 1;
	if(containersForLoading <= port.getNumberContainersPort().get()) {
	    port.getNumberContainersPort().set(port.getNumberContainersPort().get() - containersForLoading);
	} else {
	    port.getNumberContainersPort().set(0);
	}
    }
}
